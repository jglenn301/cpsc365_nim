def nim_memo(n, moves, memo):
    ''' Returns the winning move in a normal (last move wins) game of 1-row
        Nim when the legal moves are as given in moves list.  If there is
        no winning move then the return value is None.

        n -- a nonnegative integer
        moves -- a list of nonnegative integers
    '''
    if n == 0:
        return None
    if memo[n] is not None:
        # solution for n stones is already in the memo
        return memo[n]
    else:
        # search for a move that leaves our opponent with no winning move
        for m in moves:
            if m <= n and nim_memo(n - m, moves, memo) is None:
                memo[n] = m                      # record winning move in memo
                return m
        return None

for i in range(60):
    print(i, nim_memo(i, [1, 2, 3], [None] * 61))

for i in range(60):
    print(i, nim_memo(i, [1, 3, 4], [None] * 61))
