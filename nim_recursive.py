def nim_recursive(n, moves):
    ''' Returns the winning move in a normal (last move wins) game of 1-row
        Nim when the legal moves are as given in moves list.  If there is
        no winning move then the return value is None.
        WARNING: this implementation is inefficient!

        n -- a nonnegative integer
        moves -- a list of nonnegative integers
    '''
    if n == 0:
        return None
    else:
        # search for a legal move that leaves our opponent with no winning move
        for m in moves:
            if m <= n and nim_recursive(n - m, moves) is None:
                return m
        return None

for i in range(60):
    print(i, nim_recursive(i, [1, 2, 3]))

for i in range(60):
    print(i, nim_recursive(i, [1, 3, 4]))
