def nim_dp(n, moves):
    ''' Returns the winning move in a normal (last move wins) game of 1-row
        Nim when the legal moves are as given in moves list.  If there is
        no winning move then the return value is None.

        n -- a nonnegative integer
        moves -- a list of nonnegative integers
    '''
    # initialize table
    memo = [None] * (n + 1)

    # fill in table from smaller subproblems to larger
    for i in range(1, n + 1):
        for m in moves:
            if m <= i and memo[i - m] is None:
                memo[i] = m

    # return solution to initial problem
    return memo[n]
                
for i in range(60):
    print(i, nim_dp(i, [1, 2, 3]))

for i in range(60):
    print(i, nim_dp(i, [1, 3, 4]))
